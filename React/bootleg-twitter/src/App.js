import { useState, useRef, useEffect } from "react"; // 1 import useState
import TweetList from "./components/TweetList";
import { uuid } from 'uuidv4';
import './styles/App.scss'

function App() {

  const inputRef = useRef(null);

  //getter para a variavel de estado, e um setter; para alterar tem que ser com set
  const [abc, setAbc] = useState("abc");

  const [tweets, setTweets] = useState([]); // 2 define state

  //  useEffect(() => {
  //    console.log("useEffect activated")  
  //  }, [tweets]);

  const submitClickHandler = (event) => {
    event.preventDefault();
    // abc = "submit"
    setAbc("submit");
    console.log(inputRef.current.value);
    const tweetObject = {
      id: uuid(),
      text: inputRef.current.value,
      liked: false
    }
    inputRef.current.value = "";
    //const tweetText = inputRef.current.value;
    const newTweets = [...tweets, tweetObject];
    setTweets(newTweets);
  }

  //mais dificil de estilizar o hover etc; 
  const appStyles = {
    color: "white",
    backgroundColor: "Aqua"
  }
  return (
    <div className="App" style={appStyles}>
      <form>
        <input type="text" placeholder="write tweeeets here" ref={inputRef} />
        <button onClick={submitClickHandler} type="submit">{abc}</button>
      </form>
      <TweetList message="Tweets" tweets={tweets} setTweets={setTweets} />
    </div>
  );
}

export default App;
