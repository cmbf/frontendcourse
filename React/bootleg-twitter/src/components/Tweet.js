import { useState } from 'react';
import styled from 'styled-components';
import s from '../styles/Tweet.module.css'

const Tweet = ({ tweet, tweets, setTweets }) => {

    const onRemoveHandler = (event) => {
        const newTweets = tweets.filter((tweetItem) => {
            return tweetItem.id !== tweet.id;
        });
        setTweets(newTweets);
    }

    const [like, setLike] = useState(false);

    const onLikeHandler = ({ }) => {
        setLike(!like);
    }

    const TweetContainer = styled.div`
        background-color:pink
    `

    return (
        <div className={s.tweet} id={tweet.id}>
            <TweetContainer>
                <div>
                    <h3>{tweet.text}</h3>
                    <p><i className={`fas fa-heart ${tweet.like ? '' : 'hidden'}`}></i></p>
                </div>
                <button onClick={onRemoveHandler}>Remove</button>
                <button onClick={onLikeHandler}>Like</button>
            </TweetContainer>
        </div>
    );
}
export default Tweet;