import LibrarySong from "./LibrarySong";

const Library = ({ 
    songs,
    setSongs,
    currentSong,
    setCurrentSong,
    isLibraryOpen,
    isPlaying,
    setIsPlaying,
    audioRef }) => {
    return (
        <div className={`library ${isLibraryOpen ? 'open' : ''}`}>
            <h2>Library</h2>
            <div className="library-songs"></div>
            {
                songs.map((song) => {
                    return <LibrarySong 
                        song={song} 
                        songs={songs} 
                        setSongs={setSongs} 
                        currentSong={currentSong} 
                        setCurrentSong={setCurrentSong} 
                        isPlaying={isPlaying}
                        setIsPlaying={setIsPlaying}
                        audioRef={audioRef}/>
                })
            }
        </div>
    )
}

export default Library;