import Song from "./Song";

const LibrarySong = ({song,
    songs,
    setSongs,
    currentSong,
    setCurrentSong,
    isPlaying,
    setIsPlaying,
    audioRef
    }) => {
    const onSongSelectedHandler = async (event) => {
        const songId = song.id;
        const newSongs = songs.map((stateSong) =>{
            if (song.id === stateSong.id){
                return{
                    ...song,
                    active:true
                }
            } else {
                return{
                    ...stateSong,
                    active:false
                }
            }
        });
        setSongs(newSongs);
        await setCurrentSong(song);

    }
    return (
        <div className={`library-song ${song.active ? 'selected': ''}`} onClick={onSongSelectedHandler}>
            <img src={song.cover} alt={`${song.name} cover`} />
            <div className="song-description">
                <h3>Song name</h3>
                <h4>Song Artist</h4>
            </div>
        </div>
    )
}

export default LibrarySong;