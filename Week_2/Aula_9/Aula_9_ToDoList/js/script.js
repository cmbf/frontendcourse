//values
var idCount;
var todoInput;
var todoButton;
var todoList;
var filterDropdown;

$(document).ready(function () {
  // elements
  todoInput = $(".todo-input:first");
  todoButton = $("todo-btn:first");
  todoList = $(".todo-list:first");
  filterDropdown = $(".filter-todo");
});

//event listeners

todoButton.addEventListener("click", addTodo);
todoList.addEventListener("click", deleteCheck);
filterDropdown.addEventListener("change", filterTodo);

//Functions
function addTodo(event) {
  event.preventDefault();

  let todoValue = todoInput.value;
  let todoCompleteValue = false;
  idCount++;
  let todoJson = {
    value: todoValue,
    complete: todoCompleteValue,
    id: `todo-${idCount}`,
  };
  buildItemWithViewHolder(todoJson);
  save(todoJson);
  todoInput.value = "";
  saveLocalTodos(todoJson);
}

function deleteCheck(event) {
  // obter o botao da lista que foi pressionado
  var pressedButton = event.target;

  if (pressedButton.classList[0] == "trash-btn") {
    const parent = pressedButton.parentElement;
    parent.classList.add("fall");
    parent.addEventListener("transitionend", function () {
      parent.remove();
    });
  }

  if (pressedButton.classList[0] == "check-button") {
    const parent = pressedButton.parentElement;
    parent.classList.toggle("complete");
  }
}

function filterTodo(event) {
  const todos = todoList.querySelectorAll(".todo");
  console.log(event.target.value);
  todos.forEach(function (todo) {
    switch (event.target.value) {
      case "all":
        todo.style.display = "flex";
        break;
      case "completed":
        if (todo.classList.contains("complete")) {
          todo.style.display = "flex";
        } else {
          todo.style.display = "none";
        }
        break;
      case "uncompleted":
        if (!todo.classList.contains("complete")) {
          todo.style.display = "flex";
        } else {
          todo.style.display = "none";
        }
        break;
    }
  });
}

function saveLocalTodos(todo) {
  let todoItems;
  //se está no armazenamento do browser - no anónimo não funciona
  if (localStorage.getItem("todo") == null) {
    todoItems = [];
  } else {
    todoItems = JSON.parse(localStorage.getItem("todo"));
  }
  //adicionar elemento no fim da lista
  todoItems.push(todo);
  localStorage.setItem("todo", JSON.stringify(todoItems));
}

function removeTodoWithId(id) {
  let todoItems = JSON.parse(localStorage.getItem("todo"));
  todoItems = todoItems.filter(function (todoItem) {
    return todoItem.id != id;
  });
  localStorage.setItem("todo", JSON.stringify(todoItems));
}

function loadItems() {
  if (localStorage.getItem("todo") != null) {
    let todoItems = JSON.parse(localStorage.getItem("todo"));
    todoItems.map(function (todoItem) {
      //build;
    });
  }
}

function buildItemWithViewHolder(viewHolder) {
  const checkBtn = document.createElement("button");
  checkBtn.classList.add("check-button");
  checkBtn.innerHTML = '<i class="far fa-check-square"></i>';
  div.appendChild(checkBtn);
  const trashBtn = document.createElement("button");
  trashBtn.classList.add("trash-btn");
  trashBtn.innerHTML = '<i class="far fa-trash-alt"></i>';
  div.appendChild(trashBtn);
  tudoList.appendChild(div);

  // criar o div
  const div = document.createElement("div");
  div.classList.add("todo");
  div.id = viewHolder.id;
  if (viewHolder.complete) div.classList.add("complete");
  // criar o elemento da lista <li>
  const todoItem = document.createElement("li");
  todoItem.classList.add("todo-item");
  todoItem.innerHTML = todoInput.value;
  div.appendChild(todoItem);
  //criar o botão do check
  const completeBtn = document.createElement("button");
  completeBtn.classList.add("check-button");
  completeBtn.innerHTML = '<i class="fas fa-check"></i>';
  div.appendChild(completeBtn);
  // criar o botão de apagar
  // criar o botão de check
  const trashBtn = document.createElement("button");
  trashBtn.classList.add("trash-btn");
  trashBtn.innerHTML = '<i class="fas fa-trash"></i>';
  div.appendChild(trashBtn);
  //add à lista de todos
  todoList.appendChild(div);
  todoInput.value = "";
}

window.onload = function () {
  if (localStorage.getItem("idCound") == null) {
    idCount = 1;
  } else {
    idCount = JSON.parse(localStorage.getItem("idCount"));
  }
};
window.onunload = function () {
  localStorage.setItem = JSON.stringify(idCount);
};
