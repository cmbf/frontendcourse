// saving variables for later use
endPics = document.getElementsByClassName("world_ends");
imgs = document.getElementById("imgs");
popup_windows = document.getElementsByClassName("new_window");
maximizeIcon = document.getElementsByClassName("popup_max");
// defining the function that shows the hidden pictures
function showTheEnd(i) {
  endPics[i].style.display = "block";
}

/**********************   Apocalypse Pictures  **********************/

imgsLen = endPics.length;
var count = 0;
document.getElementById("btn").addEventListener("click", function () {
  document.getElementById("btn").getElementsByTagName("h2")[0].style.zIndex = 0;
  showTimer = setInterval(function () {
    console.log(count);
    showTheEnd(count);
    count++;
    if (count >= imgsLen) {
      clearInterval(showTimer);
      count = 0;
    }
  }, 100);

  /**********************   Glitch at the end  **********************/

  window.setTimeout(function () {
    var rect = imgs.getBoundingClientRect();
    console.log(rect.top, rect.right, rect.bottom, rect.left);
    imgs.style.left = rect.left - 10;
    window.setTimeout(function () {
      imgs.style.left = rect.left + 110;
      window.setTimeout(function () {
        imgs.style.left = rect.left - 20;
        window.setTimeout(function () {
          imgs.style.left = rect.left + 50;
          window.setTimeout(function () {
            imgs.style.left = rect.left - 130;
            window.setTimeout(function () {
              imgs.style.left = rect.left;
              // Add function to display windows at the end
            }, 50);
          }, 20);
        }, 30);
      }, 20);
    }, 30);
  }, 100 * imgsLen + 30);
});

/**********************   Pop-up Windows  **********************/

// Display windows one by one

// Close windows one by one

// When maximize is clicked
// When a window is opened, give it an identifier

var x = document.getElementById("myLI").parentElement;
console.log(x);
//document.getElementById("demo").innerHTML = x;
function maximizeWindow() {
  this.style.width = "100vw";
  this.style.height = "100vh";
  this.style.maxWidth = "100%";
  this.style.top = "0";
  this.style.left = "0";
}

/*
    width: 100vw;
    height: 100vw;
    max-width: 100%;
    top: 0;
    left: 0;
*/

// Error message if minimized was clicked (alert?)

/**********************   Clean The Mess  **********************/

document.getElementById("clean_mess").addEventListener("click", function () {
  console.log(this);
  document
    .getElementById("btn")
    .getElementsByTagName("h2")[0].style.zIndex = 50;
  for (i = 0; i < imgsLen; i++) {
    console.log(i);
    endPics[i].style.display = "none";
  }

  for (i = 0; i < imgsLen; i++) {
    popup_windows[i].style.display = "none";
  }

  // Hide windows and send them backwards
});
