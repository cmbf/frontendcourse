var h1 = document.getElementsByTagName('h1')[0];
var plus = document.getElementById('plus');
var minus = document.getElementById('minus');

var number = 0;

plus.addEventListener('click', function(){
    number ++;
    console.log(number);
    h1.innerHTML = number.toString();
});

minus.addEventListener('click', function(){
    number --;
    console.log(number);
    h1.innerHTML = number.toString();
});

console.log(document.getElementsByTagName('body')[0]);

/*
var nr = document.getElementById('nr');
var plus = document.getElementById('plus');
var minus = document.getElementById('minus');

var number = 0;

plus.addEventListener('click', function(){
    number ++;
    console.log(number);
    nr.innerHTML = number.toString();
});

minus.addEventListener('click', function(){
    number --;
    console.log(number);
    nr.innerHTML = number.toString();
});

console.log(document.getElementsByTagName('body')[0]);
*/