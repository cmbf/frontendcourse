
function whileFunction(end){
    var count = 0;
    while(count<10){
        console.log(count);
        count++;
    }
}

function doWhileFunction(end){
    var count = 0;
    do {
        console.log(count);
        count++;
        
    } while (count < end);
}

function fori(end){
    for(var i = 0; i < end; i++){
        console.log(i);
    }
}

window.onload = fori(10);